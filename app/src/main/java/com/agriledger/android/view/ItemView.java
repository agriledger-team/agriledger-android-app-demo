package com.agriledger.android.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.agriledger.android.R;
import com.agriledger.android.util.L;

/**
 * User: Pavel
 * Date: 02.09.2016
 * Time: 11:18
 */
public class ItemView extends RelativeLayout {

    /*************************************
     * BINDS
     *************************************/
    @BindView(R.id.item_title)
    TextView title;
    @BindView(R.id.item_desc)
    TextView desc;
    @BindView(R.id.item_icon)
    ImageView icon;

    /*************************************
     * PRIVATE FIELDS
     *************************************/
    private String mTitleString;
    private String mDescriptionString;
    //private Paint mPaint;
    private int mPosition;
    private View customView;

    /*************************************
     * PUBLIC METHODS
     *************************************/
    public ItemView(Context context) {
        super(context);
        init(context);
    }

    public ItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public ItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs);
    }

    public void setTitle(String title) {
        mTitleString = title;
        if (this.title != null) {
            this.title.setText(mTitleString);
        }
    }

    public void setDescription(String description) {
        mDescriptionString = description;
        if (this.desc != null) {
            this.desc.setText(mDescriptionString);
        }
    }

    public void setTitleColorResId(int colorResId) {
        L.i("setTitleColorResId", colorResId);
        if (this.title != null) {
            this.title.setTextColor(getResources().getColor(colorResId));
        }
    }

    public void setIcon(int imgResource) {
        if (icon != null) {
            icon.setImageResource(imgResource);
            icon.setVisibility(VISIBLE);
        }
    }

    public void setIconSize(int iconSize) {
        if (icon != null) {
            ViewGroup.LayoutParams params = icon.getLayoutParams();
            params.height = iconSize;
            params.width = iconSize;
            icon.setLayoutParams(params);
        }
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    public int getPosition() {
        return mPosition;
    }

    public void setReady() {
        L.i("setReady");
        //setBackgroundResource(R.drawable.selector_orange_item);
        /**
         * Pavel:
         * initial spec told us make it firstly orange, then green
         * But it seems too complicated for the user.
         * Lets do it green all the way.
         */
        setBackgroundResource(R.drawable.selector_green_item);
    }

    public void setConfirmed() {
        L.i("setConfirmed");
        setBackgroundResource(R.drawable.selector_green_item);
    }

    public void setIdle() {
        L.i("set");
        setBackgroundResource(R.drawable.selector_brown_item);
    }

    public void showCustomView(View customView) {
        L.i("showCustomView");
        title.setVisibility(GONE);
        desc.setVisibility(GONE);
        icon.setVisibility(GONE);
        this.customView = customView;
        addView(this.customView);
    }

    /*************************************
     * PRIVATE METHODS
     *************************************/
    private void init(Context context) {
        //LLog.e(TAG, "init");
        inflate(context, R.layout.view_item, this);
        ButterKnife.bind(this, this);
        setClickable(true);
        setBackgroundResource(R.drawable.selector_brown_item);

        if (mTitleString != null && !mTitleString.equals("")) {
            this.title.setText(mTitleString);
        }

    }

    private void init(Context context, AttributeSet attributes) {
        //LLog.e(TAG, "init attrs");
        init(context);

        TypedArray styledAttributes = getContext().obtainStyledAttributes(attributes, R.styleable.ItemView);
        int attrsCount = styledAttributes.getIndexCount();

        for (int i = 0; i < attrsCount; i++) {
            int styledAttributesIndex = styledAttributes.getIndex(i);

            switch (styledAttributesIndex) {
                case R.styleable.ItemView_itemTitle:
                    title.setText(styledAttributes.getString(styledAttributesIndex));
                    break;
                case R.styleable.ItemView_itemDesc:
                    desc.setText(styledAttributes.getString(styledAttributesIndex));
                    if (!desc.getText().equals("")) {
                        desc.setVisibility(VISIBLE);
                    }
                    break;
                case R.styleable.ItemView_itemLast:
                    if (styledAttributes.getBoolean(styledAttributesIndex, false)) {
                        //setBackgroundResource(R.color.temp);
                    }
                    break;
                case R.styleable.ItemView_itemIcon:
                    if (styledAttributes.getResourceId(styledAttributesIndex, 0) > 0) {
                        icon.setImageResource(styledAttributes.getResourceId(styledAttributesIndex, 0));
                        icon.setVisibility(VISIBLE);
                    }
                    break;
                case R.styleable.ItemView_itemAllCaps:
                    title.setAllCaps(styledAttributes.getBoolean(styledAttributesIndex, false));
                    break;
            }
        }

        styledAttributes.recycle();
    }

    private void alignTitleCenterVertical() {
        L.i("alignTitleCenterVertical");
        LayoutParams params = (LayoutParams) title.getLayoutParams();
        params.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
        title.setLayoutParams(params);
    }

    public void hideCustomView() {
        L.i("hideCustomView");
        customView.setVisibility(GONE);
        title.setVisibility(VISIBLE);
        desc.setVisibility(VISIBLE);
        icon.setVisibility(VISIBLE);
    }
}

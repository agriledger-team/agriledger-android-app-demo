package com.agriledger.android.managers;

import android.media.MediaPlayer;
import android.util.Log;
import com.agriledger.android.App;
import com.agriledger.android.util.L;

/**
 * User: Pavel
 * Date: 29.08.2016
 * Time: 18:46
 */
public class SoundManager {

    /************************************
     * PRIVATE STATIC FIELDS
     ************************************/
    private static SoundManager instance;

    /************************************
     * PRIVATE FIELDS
     ************************************/
    private MediaPlayer mediaPlayer;

    /************************************
     * PUBLIC STATIC METHODS
     ************************************/
    public static SoundManager getInstance() {
        L.i("getInstance");
        if (instance == null) {
            instance = new SoundManager();
        }
        return instance;
    }

    /************************************
     * PUBLIC METHODS
     ************************************/
    public void playSound(int soundResId) {
        L.i("playSound");

        try {
            mediaPlayer.stop();
        } catch (Exception e) {
            L.e("playSound - Exception - " + Log.getStackTraceString(e));
        }
        mediaPlayer = MediaPlayer.create(App.getContext(), soundResId);
        mediaPlayer.start();
    }

    public void release() {
        L.i("release");
        if (mediaPlayer == null) {
            return;
        }
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        mediaPlayer.reset();
        mediaPlayer.release();
        mediaPlayer = null;
    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private SoundManager() {
        L.i("SoundManager");
        mediaPlayer = new MediaPlayer();
    }

    private void init() {
        L.i("init");
    }

}

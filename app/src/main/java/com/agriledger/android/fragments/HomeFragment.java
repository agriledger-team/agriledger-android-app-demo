package com.agriledger.android.fragments;

import android.view.View;
import android.widget.ImageButton;
import butterknife.BindView;
import butterknife.OnClick;
import com.agriledger.android.R;
import com.agriledger.android.managers.SoundManager;
import com.agriledger.android.util.L;

/**
 * User: Pavel
 * Date: 29.08.2016
 * Time: 18:06
 */
public class HomeFragment extends BaseFragment {

    /************************************
     * BINDS
     ************************************/
    @BindView(R.id.receive_button)
    ImageButton receiveButton;

    /************************************
     * PROTECTED METHODS
     ************************************/
    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void init(View rootView) {
        super.init(rootView);
        L.i("init");
        SoundManager.getInstance().playSound(R.raw.welcome);
        showToolbar();
        setToolbarIcon(R.drawable.agriledger_horizontal_logo);
        receiveButton.setAlpha(1.0f);
    }

    /************************************
     * PUBLIC METHODS
     ************************************/
    @OnClick(R.id.receive_button)
    public void onReceiveButtonClick() {
        L.i("onReceiveButtonClick");
        mainActivity.showReceiveScreen();
    }

    @OnClick(R.id.planning_button)
    public void onPlanningButtonClick() {
        L.i("onPlanningButtonClick");
        SoundManager.getInstance().playSound(R.raw.section_is_in_development_planning);
    }

    @OnClick(R.id.basket_button)
    public void onBasketButtonClick() {
        L.i("onBasketButtonClick");
        SoundManager.getInstance().playSound(R.raw.section_is_in_development_basket);
    }

    @OnClick(R.id.selling_button)
    public void onSellingButtonClick() {
        L.i("onSellingButtonClick");
        SoundManager.getInstance().playSound(R.raw.section_is_in_development_selling);
    }

    @OnClick(R.id.shopping_button)
    public void onShoppingButtonClick() {
        L.i("onShoppingButtonClick");
        SoundManager.getInstance().playSound(R.raw.section_is_in_development_shopping);
    }

    @OnClick(R.id.wallet_button)
    public void onWalletButtonClick() {
        SoundManager.getInstance().playSound(R.raw.section_is_in_development_wallet);
        L.i("onWalletButtonClick");
    }

    public void onInfoButtonClick() {
        L.i("onInfoButtonClick");
        SoundManager.getInstance().playSound(R.raw.welcome);
    }

}

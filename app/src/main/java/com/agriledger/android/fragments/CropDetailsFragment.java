package com.agriledger.android.fragments;

import android.graphics.PorterDuff;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import butterknife.BindView;
import butterknife.OnClick;
import com.agriledger.android.App;
import com.agriledger.android.R;
import com.agriledger.android.managers.QrManager;
import com.agriledger.android.managers.SoundManager;
import com.agriledger.android.model.Cooperator;
import com.agriledger.android.model.Crop;
import com.agriledger.android.util.L;
import com.agriledger.android.view.ItemView;

/**
 * User: Pavel
 * Date: 29.08.2016
 * Time: 18:07
 */
public class CropDetailsFragment extends BaseFragment {

    /************************************
     * PUBLIC STATIC CONSTANTS
     ************************************/
    public static final int WAITING_TIME = 3000;

    /************************************
     * BINDS
     ************************************/
    @BindView(R.id.crop_item)
    ItemView cropItem;
    @BindView(R.id.cooperator_item)
    ItemView cooperatorItem;
    @BindView(R.id.scan_button)
    ImageButton scanButton;
    @BindView(R.id.transfer_button)
    ImageButton transferButton;
    @BindView(R.id.show_qr_button)
    ImageButton showQrButton;
    @BindView(R.id.qr_code_layout)
    RelativeLayout qrCodeLayout;
    @BindView(R.id.qr_code_view)
    ImageView qrCodeView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.done_button)
    ImageButton doneButton;

    /************************************
     * PRIVATE FIELDS
     ************************************/
    private int cooperatorState;
    private int transferState;
    private Animation flashingAnimation;

    /************************************
     * PUBLIC METHODS
     ************************************/
    @OnClick({R.id.cooperator_item, R.id.crop_item})
    public void onItemClick() {
        L.i("onItemClick");
        playInfo();
    }

    @OnClick(R.id.done_button)
    public void onDoneButtonClick() {
        L.i("onDoneButtonClick");
        hideQrCode();
        progressBar.setVisibility(View.VISIBLE);
        play(R.raw.waiting_for_info_who_scanned_your_code);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                play(R.raw.cooperator_scanned_you_qr_code);
                Cooperator cooperator = Cooperator.createCooperator();
                cooperatorItem.setTitle(cooperator.name);
                cooperatorItem.setIcon(R.drawable.cooperator_photo);
                cooperatorItem.setDescription(cooperator.description + "\n" + "AgID " + cooperator.id);
                cooperatorItem.hideCustomView();
                setCooperatorState(CooperatorState.RECOGNIZED);
            }
        }, WAITING_TIME);

    }

    @OnClick(R.id.show_qr_button)
    public void onShowQrButtonClick() {
        L.i("onShowQrButtonClick");
        showQrCode();
    }

    @OnClick(R.id.scan_button)
    public void onScanButtonClick() {
        L.i("onScanButtonClick");
        QrManager.scanQrCode(getActivity());
    }

    @OnClick(R.id.transfer_button)
    public void onTransferButtonClick() {
        L.i("onTransferButtonClick");
        transferState = TransferState.TRANSFERRED;
        stopBlinking(transferButton);
        transferButton.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        play(R.raw.produce_transferred);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                transferState = TransferState.ACCEPTED;
                play(R.raw.transfer_has_been_accepted);
                progressBar.setVisibility(View.GONE);
                cropItem.setConfirmed();
                mainActivity.setDealComplete(true);
            }
        }, WAITING_TIME);
    }

    public void onInfoButtonClick() {
        L.i("onInfoButtonClick");
        playInfo();
        /*switch (cooperatorState) {
            case CooperatorState.IDLE:
                if (qrCodeLayout.getVisibility() == View.GONE) {
                    play(R.raw.this_is_produce_details_page_0);
                } else {
                    play(R.raw.this_is_produce_details_page_1);
                }
                break;
            case CooperatorState.RECOGNIZED:
                play(R.raw.this_is_produce_details_page_2);
                break;
            case CooperatorState.CONFIRMED:
                switch (transferState) {
                    case TransferState.IDLE:
                    case TransferState.TRANSFERRED:
                        play(R.raw.this_is_produce_details_page_3);
                        break;
                    case TransferState.ACCEPTED:
                        play(R.raw.this_is_produce_details_page_4);
                        break;
                }
                break;
        }*/
    }

    public void onQrCodeScanned() {
        L.i("onQrCodeScanned");
        play(R.raw.cooperator_confirmed);
        setCooperatorState(CooperatorState.CONFIRMED);
    }

    /************************************
     * PROTECTED METHODS
     ************************************/
    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_crop_detail;
    }

    @Override
    protected void init(View rootView) {
        L.i("init");
        super.init(rootView);
        showToolbar();
        setToolbarIcon(R.drawable.ic_receive);
        play(R.raw.show_you_qr_code);
        transferState = TransferState.IDLE;

        Crop riverCrop = Crop.createRiverFieldCrop();
        cropItem.setTitle(riverCrop.fieldName);
        cropItem.setDescription(
                riverCrop.productName + "\n" +
                        riverCrop.harvestDate + "\n" +
                        (riverCrop.isReady ? "Ready!" : "")
        );
        cropItem.setIcon(R.drawable.field_river);
        if (riverCrop.isReady) {
            cropItem.setReady();
        }

        setCooperatorState(CooperatorState.IDLE);

        progressBar.getIndeterminateDrawable().setColorFilter(App.color(R.color.brown_dark), PorterDuff.Mode.MULTIPLY);

    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private void setCooperatorState(int cooperatorState) {
        L.i("setCooperatorState");
        progressBar.setVisibility(View.GONE);
        this.cooperatorState = cooperatorState;
        switch (cooperatorState) {
            case CooperatorState.IDLE:
                startBlinking(showQrButton);
                showQrButton.setVisibility(View.VISIBLE);
                scanButton.setVisibility(View.GONE);
                transferButton.setVisibility(View.GONE);
                cooperatorItem.showCustomView(createCustomViewForCooperatorItem());
                break;
            case CooperatorState.RECOGNIZED:
                stopBlinking(showQrButton);
                showQrButton.setVisibility(View.GONE);
                startBlinking(scanButton);
                scanButton.setVisibility(View.VISIBLE);
                transferButton.setVisibility(View.GONE);
                //cooperatorItem.setReady();
                break;
            case CooperatorState.CONFIRMED:
                stopBlinking(showQrButton);
                showQrButton.setVisibility(View.GONE);
                stopBlinking(scanButton);
                scanButton.setVisibility(View.GONE);
                startBlinking(transferButton);
                transferButton.setVisibility(View.VISIBLE);
                cooperatorItem.setConfirmed();
                break;
        }
    }

    private View createCustomViewForCooperatorItem() {
        L.i("createCustomViewForCooperatorItem");

        TextView questionTextView = new TextView(getContext());
        questionTextView.setText("?");
        questionTextView.setTextSize(100);
        questionTextView.setTextColor(App.color(R.color.brown_dark_very));
        questionTextView.setPadding(0, -30, 0, 0);
        questionTextView.setGravity(Gravity.CENTER);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );
        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        questionTextView.setLayoutParams(params);

        return questionTextView;
    }

    private void showQrCode() {
        L.i("showQrCode");

        scanButton.setVisibility(View.GONE);
        transferButton.setVisibility(View.GONE);
        stopBlinking(showQrButton);
        showQrButton.setVisibility(View.GONE);

        cooperatorItem.setVisibility(View.GONE);
        cropItem.setVisibility(View.GONE);
        qrCodeLayout.setVisibility(View.VISIBLE);
        startBlinking(doneButton);

        if (mainActivity.getFarmer().qrCode != null) {
            qrCodeView.setImageBitmap(mainActivity.getFarmer().qrCode);
        } else {
            qrCodeView.setImageBitmap(QrManager.generateQrCode(mainActivity.getFarmer().name));
        }

        play(R.raw.press_the_flashing_button_after_the_scan);

    }

    private void hideQrCode() {
        L.i("hideQrCode");
        cooperatorItem.setVisibility(View.VISIBLE);
        cropItem.setVisibility(View.VISIBLE);
        qrCodeLayout.setVisibility(View.GONE);
        stopBlinking(doneButton);
    }

    private void startBlinking(View view) {
        L.i("startBlinking");
        flashingAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.flashing_animation);
        view.startAnimation(flashingAnimation);
    }

    private void stopBlinking(View view) {
        L.i("stopBlinking");
        view.clearAnimation();
    }

    private void play(int soundResId) {
        L.i("play");
        SoundManager.getInstance().playSound(soundResId);
    }

    private void playInfo() {
        L.i("playInfo");
        switch (cooperatorState) {
            case CooperatorState.IDLE:
                if (qrCodeLayout.getVisibility() == View.VISIBLE) {
                    play(R.raw.press_the_flashing_button_after_the_scan);
                } else {
                    play(R.raw.show_qr_code_to_cooperator);
                }
                break;
            case CooperatorState.RECOGNIZED:
                play(R.raw.scan_cooperators_code_now);
                break;
            case CooperatorState.CONFIRMED:
                switch (transferState) {
                    case TransferState.IDLE:
                        play(R.raw.transfer_produce_to_cooperator);
                        break;
                    case TransferState.TRANSFERRED:
                        play(R.raw.waiting_cooperator_accept_transfer);
                        break;
                    case TransferState.ACCEPTED:
                        play(R.raw.deal_is_complete);
                        break;
                }
                break;
        }
    }

    /************************************
     * PUBLIC INTERFACES
     ************************************/
    public interface CooperatorState {

        int IDLE = 0;
        int RECOGNIZED = 1;
        int CONFIRMED = 2;

    }

    public interface TransferState {

        int IDLE = 0;
        int TRANSFERRED = 1;
        int ACCEPTED = 2;

    }

}

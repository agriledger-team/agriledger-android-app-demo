package com.agriledger.android.model;

import android.graphics.Bitmap;
import com.agriledger.android.util.L;

/**
 * User: Pavel
 * Date: 29.08.2016
 * Time: 14:02
 */
public class Farmer {

    /************************************
     * PUBLIC FIELDS
     ************************************/
    public String name;
    public String id;
    public Bitmap qrCode;

    /************************************
     * PUBLIC STATIC METHODS
     ************************************/
    public static Farmer createFarmer() {
        L.i("createCooperator");
        Farmer farmer = new Farmer();
        farmer.name = "Zuku";
        farmer.id = "s098s0f9dv8cx";
        return farmer;
    }

    /************************************
     * PUBLIC METHODS
     ************************************/
    public Farmer() {
    }

    public Farmer(String name) {
        this.name = name;
    }

}

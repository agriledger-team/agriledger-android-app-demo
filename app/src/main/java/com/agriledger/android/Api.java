package com.agriledger.android;

import com.agriledger.android.model.Farmer;
import com.agriledger.android.util.L;
import com.backendless.Backendless;
import com.backendless.Subscription;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.messaging.Message;
import com.backendless.messaging.MessageStatus;

import java.util.List;

/**
 * User: Pavel
 * Date: 29.08.2016
 * Time: 15:05
 */
public class Api {

    public static void login(int code) {
        L.i("login");
        /**
         * send the code to the server,
         * got all users
         * if found user with the code - returns it
         * save the name and id in the prefs
         * then used it every time
         */
    }

    public static void getCrops(String farmerId) {
        L.i("getCrops", farmerId);

    }

    public static void farmerRecognized(String farmerId, String cooperatorId) {
        L.i("farmerScanned");
        /**
         * send the fact of recognizing the farmer
         */
    }

    public static void cooperatorRecognized(String farmerId, String cooperatorId) {
        L.i("cooperatorScanned");
        /**
         * send the fact of recognizing the cooperator
         */
    }

    public static void transfer(String goodId, String farmerId, String cooperatorId) {
        L.i("transferGoodsToCooperator");
    }

    public static void acceptTransfer(String goodId, String farmerId, String cooperatorId) {
        L.i("acceptTransfer");
    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private void subscribeToMessages() {
        L.i("subscribeToMessages");

        AsyncCallback<List<Message>> messagesCallback = new AsyncCallback<List<Message>>() {

            public void handleResponse(List<Message> response) {
                L.i("subscribeToMessages - handleResponse", response.get(0).getData());

            }

            public void handleFault(BackendlessFault fault) {
                L.i("subscribeToMessages - handleFault", fault.getMessage());
            }

        };

        AsyncCallback<Subscription> subscriptionCallback = new AsyncCallback<Subscription>() {

            public void handleResponse(Subscription response) {
                L.i("handleResponse", response.getSubscriptionId());
                //subscribed = true;
            }

            public void handleFault(BackendlessFault fault) {
                L.i("handleFault", fault.getMessage());
                //subscribed = false;
            }

        };

        Backendless.Messaging.subscribe(messagesCallback, subscriptionCallback);

    }

    private void sendMessageToBackend() {
        L.i("sendMessageToBackend");

        Farmer farmer = new Farmer("Mgabe");
        Backendless.Messaging.publish(farmer, new AsyncCallback<MessageStatus>() {
            @Override
            public void handleResponse(MessageStatus messageStatus) {
                L.i("sendMessageToBackend - handleResponse", messageStatus.getStatus());
            }

            @Override
            public void handleFault(BackendlessFault backendlessFault) {
                L.i("sendMessageToBackend - handleFault", backendlessFault.getMessage());
            }
        });

    }

    /************************************
     * PUBLIC INTERFACES
     ************************************/
    public interface GetUsersCallback {

    }

}

package com.agriledger.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.agriledger.android.fragments.CropDetailsFragment;
import com.agriledger.android.fragments.HomeFragment;
import com.agriledger.android.fragments.LoginFragment;
import com.agriledger.android.fragments.ReceiveFragment;
import com.agriledger.android.managers.SoundManager;
import com.agriledger.android.model.Farmer;
import com.agriledger.android.util.L;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class MainActivity extends ActionBarActivity {

    /************************************
     * BINDS
     ************************************/
    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    public TextView title;
    @BindView(R.id.toolbar_icon)
    public ImageView toolbarIcon;

    /************************************
     * PRIVATE FIELDS
     ************************************/
    private boolean subscribed;
    private int screen;
    private HomeFragment homeFragment;
    private ReceiveFragment receiveFragment;
    private CropDetailsFragment cropDetailsFragment;
    private boolean isDealComplete = false;
    private Farmer farmer;

    /************************************
     * PUBLIC METHODS
     ************************************/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        L.i("onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setTitle("");
        showLoginScreen();

        //generateQrCode();
        //scanQrCode();

        /*subscribeToMessages();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sendMessageToBackend();
            }
        }, 2000);*/
    }

    @Override
    protected void onDestroy() {
        L.i("onDestroy");
        super.onDestroy();
        SoundManager.getInstance().release();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        L.i("onActivityResult");
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                L.i("onActivityResult", "Cancelled");
            } else {
                if (cropDetailsFragment != null) {
                    cropDetailsFragment.onQrCodeScanned();
                }
                L.i("onActivityResult", "Scanned: " + result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        L.i("onBackPressed", screen);
        switch (screen) {
            case Screen.HOME:
                showLoginScreen();
                break;
            case Screen.LOGIN:
                super.onBackPressed();
                break;
            case Screen.RECEIVE:
            case Screen.INFO:
            default:
                showHomeScreen();
                break;
            case Screen.CROP_DETAILS:
                showReceiveScreen();
                break;
        }
    }

    @OnClick(R.id.back_button)
    public void onBackButtonClick() {
        L.i("onBackButtonClick");
        onBackPressed();
    }

    @OnClick(R.id.info_button)
    public void onInfoButtonClick() {
        L.i("onInfoButtonClick");
        switch (screen) {
            case Screen.HOME:
                if (homeFragment != null) {
                    homeFragment.onInfoButtonClick();
                }
                break;
            case Screen.RECEIVE:
                if (receiveFragment != null) {
                    receiveFragment.onInfoButtonClick();
                }
                break;
            case Screen.CROP_DETAILS:
                if (cropDetailsFragment != null) {
                    cropDetailsFragment.onInfoButtonClick();
                }
                break;
        }
    }

    public void showLoginScreen() {
        L.i("showPaymentScreen");
        screen = Screen.LOGIN;
        showFragment(new LoginFragment(), false);
    }

    public void showHomeScreen() {
        L.i("showHomeScreen");
        screen = Screen.HOME;
        homeFragment = new HomeFragment();
        showFragment(homeFragment, false);
    }

    public void showInfoScreen() {
        L.i("showInfoScreen");
        //screen = Screen.INFO;
        /**
         * TODO:
         * that add here info screen showing
         */
        //showFragment(new HomeFragment(), false);
    }

    public void showReceiveScreen() {
        L.i("showReceiveScreen");
        screen = Screen.RECEIVE;
        receiveFragment = new ReceiveFragment();
        showFragment(receiveFragment, false);
    }

    public void showCropDetailsScreen() {
        L.i("showCropDetailsScreen");
        screen = Screen.CROP_DETAILS;
        cropDetailsFragment = new CropDetailsFragment();
        showFragment(cropDetailsFragment, false);
    }

    public void showTitle() {
        L.i("showTitle");
        if (title != null) {
            title.setVisibility(View.VISIBLE);
        }
    }

    public void playSound(int soundResId) {
        L.i("playSound");
    }

    public void setDealComplete(boolean isDealComplete) {
        L.i("setDealComplete");
        this.isDealComplete = isDealComplete;
    }

    public boolean isDealComplete() {
        L.i("isDealComplete");
        return isDealComplete;
    }

    public void setFarmer(Farmer farmer) {
        L.i("getFarmer");
        this.farmer = farmer;
    }

    public Farmer getFarmer() {
        L.i("setFarmer");
        return farmer;
    }

    /************************************
     * PRIVATE METHODS
     ************************************/
    private void showFragment(Fragment fragment, boolean addToBackStack) {
        L.i("showFragment", fragment.getClass().getSimpleName(), addToBackStack);

        String tag = fragment.getClass().getSimpleName();
        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.container, fragment, tag);
        if (addToBackStack) {
            transaction.addToBackStack(tag);
        }

        transaction.commitAllowingStateLoss();
    }

    /************************************
     * PUBLIC INTERFACES
     ************************************/
    public interface Screen {

        int LOGIN = 0;
        int HOME = 1;
        int RECEIVE = 2;
        int CROPS = 3;
        int CROP_DETAILS = 4;
        int SHOW_QR = 5;
        int SCAN_QR = 6;
        int INFO = 7;

    }

}
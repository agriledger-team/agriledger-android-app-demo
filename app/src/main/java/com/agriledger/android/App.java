package com.agriledger.android;

import android.app.Application;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;
import com.agriledger.android.api.MyBackendless;
import com.agriledger.android.util.L;
import com.backendless.Backendless;

/**
 * User: Pavel
 * Date: 13.07.2016
 * Time: 19:31
 */
public class App extends Application {

    private static App sInstance;

    @Override
    public void onCreate() {
        L.i("onCreate");
        super.onCreate();
        L.plant(new L.DebugTree());
        Backendless.initApp(this, MyBackendless.APP_ID, MyBackendless.SECRET_KEY, "v1");
        sInstance = this;
    }

    public static Context getContext() {
        return sInstance;
    }

    public static int color(int colorResId) {
        return ContextCompat.getColor(getContext(), colorResId);
    }

    public static void toast(String str) {
        Toast.makeText(sInstance, str, Toast.LENGTH_SHORT).show();
    }

    public static void toast(int strResId) {
        Toast.makeText(sInstance, strResId, Toast.LENGTH_SHORT).show();
    }

}

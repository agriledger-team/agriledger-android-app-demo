package com.agriledger.android.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.*;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.agriledger.android.App;
import com.agriledger.android.MainActivity;
import com.agriledger.android.R;
import com.agriledger.android.util.L;

/**
 * User: Pavel
 * Date: 25.07.2016
 * Time: 6:37
 */
public abstract class BaseFragment extends Fragment {

    /************************************
     * PROTECTED FIELDS
     ************************************/
    protected MainActivity mainActivity;
    protected Unbinder unbinder;

    @Override
    public void onAttach(Context context) {
        L.i("onAttach");
        super.onAttach(context);
        mainActivity = (MainActivity) context;
    }

    @Override
    public void onDetach() {
        L.i("onDetach");
        //mainActivity = null;
        super.onDetach();
    }

    /************************************
     * PUBLIC METHODS
     ************************************/
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        L.i("onCreateView");
        View rootView = inflater.inflate(getLayoutResId(), container, false);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        L.i("onDestroyView");
        release();
        super.onDestroyView();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void showToolbar() {
        L.i("showToolbar");
        if (mainActivity.toolbar != null) {
            mainActivity.toolbar.setVisibility(View.VISIBLE);
        }
    }

    public void hideToolbar() {
        L.i("hideToolbar");
        if (mainActivity.toolbar != null) {
            mainActivity.toolbar.setVisibility(View.GONE);
        }
    }

    public void setToolbarTitle(String title) {
        L.i("setToolbarTitle");
        if (mainActivity != null) {
            mainActivity.title.setText(title);
        }
    }

    public void setToolbarIcon(int toolbarIconResId) {
        L.i("setToolbarIcon");
        mainActivity.toolbarIcon.setImageResource(toolbarIconResId);
    }

    public void onInfoButtonClick() {
        L.i("onInfoButtonClick");
    }

    /************************************
     * PROTECTED METHODS
     ************************************/
    protected int getLayoutResId() {
        return 0;
    }

    protected void init(View rootView) {
        L.i("init");

        hideToolbar();
        mainActivity.showTitle();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            window.setStatusBarColor(App.color(getSystemBarColor()));
        }

    }

    protected void release() {
        L.i("release");
        unbinder.unbind();
    }

    @ColorRes
    protected int getSystemBarColor() {
        return R.color.color_primary_dark;
    }



}
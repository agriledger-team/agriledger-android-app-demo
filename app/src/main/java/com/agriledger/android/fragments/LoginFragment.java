package com.agriledger.android.fragments;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.OnClick;
import com.agriledger.android.R;
import com.agriledger.android.managers.QrManager;
import com.agriledger.android.managers.SoundManager;
import com.agriledger.android.model.Farmer;
import com.agriledger.android.util.L;

/**
 * User: Pavel
 * Date: 29.08.2016
 * Time: 18:06
 */
public class LoginFragment extends BaseFragment implements View.OnClickListener {

    public static final int CODE_FARMER = 1111;
    public static final int CODE_COOPERATOR = 2222;

    /************************************
     * BINDS
     ************************************/
    @BindViews({R.id.one, R.id.two, R.id.three,
            R.id.four, R.id.five, R.id.six,
            R.id.seven, R.id.eight, R.id.nine,
            R.id.zero})
    Button[] buttons;
    @BindView(R.id.code_input)
    TextView codeInput;

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_login;
    }

    @Override
    protected void init(View rootView) {
        L.i("init");
        super.init(rootView);
        SoundManager.getInstance().playSound(R.raw.enter_login_code);
        for (Button b : buttons) {
            b.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        L.i("onClick");

        if (codeInput.getText().length() >= 4) {
            return;
        }

        switch (view.getId()) {
            case R.id.zero:
                codeInput.append("0");
                break;
            case R.id.one:
                codeInput.append("1");
                break;
            case R.id.two:
                codeInput.append("2");
                break;
            case R.id.three:
                codeInput.append("3");
                break;
            case R.id.four:
                codeInput.append("4");
                break;
            case R.id.five:
                codeInput.append("5");
                break;
            case R.id.six:
                codeInput.append("6");
                break;
            case R.id.seven:
                codeInput.append("7");
                break;
            case R.id.eight:
                codeInput.append("8");
                break;
            case R.id.nine:
                codeInput.append("9");
                break;
        }
    }

    @OnClick(R.id.confirm_button)
    public void onConfirmButtonClick() {
        L.i("onConfirmButtonClick");

        if (codeInput.getText().toString().equals("") || codeInput.getText().length() != 4) {
            return;
        }

        int code = Integer.parseInt(codeInput.getText().toString());
        switch (code) {
            case CODE_COOPERATOR:
                break;
            case CODE_FARMER:
                break;
        }

        /**
         * Pavel:
         *
         * we are simulating the login process here,
         * so we will create a farmer right here, and generate his qr code
         * in the background thread.
         *
         */
        mainActivity.setFarmer(Farmer.createFarmer());
        new GenerateQrCodeAsyncTask().execute(mainActivity.getFarmer().name);

        mainActivity.showHomeScreen();

    }

    @OnClick(R.id.back_button)
    public void onBackButtonClick() {
        L.i("onBackButtonClick");
        String code = codeInput.getText().toString();
        if (code.length() == 0) {
            return;
        }
        codeInput.setText(code.substring(0, code.length() - 1));
    }

    private void validateCode(int code) {
        L.i("validateCode");

        //switch (code)

    }


    /************************************
     * PUBLIC CLASSES
     ************************************/
    public class GenerateQrCodeAsyncTask extends AsyncTask<String, String, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... strings) {
            L.i("doInBackground");
            return QrManager.generateQrCode(strings[0]);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            mainActivity.getFarmer().qrCode = bitmap;
        }
    }

}

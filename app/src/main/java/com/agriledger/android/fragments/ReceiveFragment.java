package com.agriledger.android.fragments;

import android.view.View;
import butterknife.BindView;
import butterknife.OnClick;
import com.agriledger.android.R;
import com.agriledger.android.managers.SoundManager;
import com.agriledger.android.model.Crop;
import com.agriledger.android.util.L;
import com.agriledger.android.view.ItemView;

/**
 * User: Pavel
 * Date: 02.09.2016
 * Time: 10:46
 */
public class ReceiveFragment extends BaseFragment {

    /************************************
     * BINDS
     ************************************/
    @BindView(R.id.field_item_0)
    ItemView fieldItem0;
    @BindView(R.id.field_item_1)
    ItemView fieldItem1;

    /************************************
     * PROTECTED METHODS
     ************************************/
    @OnClick(R.id.field_item_1)
    public void onFieldItemOneClick() {
        L.i("onFieldItemOneClick");
        SoundManager.getInstance().playSound(R.raw.produce_is_not_ready);
    }

    @OnClick(R.id.field_item_0)
    public void onFieldItemZeroClick() {
        L.i("onFieldItemZeroClick");
        mainActivity.showCropDetailsScreen();
    }

    public void onInfoButtonClick() {
        L.i("onInfoButtonClick");
        SoundManager.getInstance().playSound(R.raw.select_produce_to_receive);
    }

    /************************************
     * PROTECTED METHODS
     ************************************/
    @Override
    protected void init(View rootView) {
        L.i("init");
        super.init(rootView);
        showToolbar();
        setToolbarIcon(R.drawable.ic_receive);
        SoundManager.getInstance().playSound(R.raw.select_produce_to_receive);

        Crop riverCrop = Crop.createRiverFieldCrop();
        fieldItem0.setTitle(riverCrop.fieldName);
        fieldItem0.setDescription(
                riverCrop.productName + "\n" +
                        riverCrop.harvestDate +
                        (riverCrop.isReady ? "\nReady!" : ""));
        fieldItem0.setIcon(R.drawable.field_river);
        if (riverCrop.isReady) {
            fieldItem0.setReady();
        }

        Crop forestCrop = Crop.createForestFieldCrop();
        fieldItem1.setTitle(forestCrop.fieldName);
        fieldItem1.setDescription(
                forestCrop.productName + "\n" +
                        forestCrop.harvestDate +
                        (forestCrop.isReady ? "\nReady!" : ""));
        fieldItem1.setIcon(R.drawable.field_forest);
        if (forestCrop.isReady) {
            fieldItem1.setReady();
        }

        if (mainActivity.isDealComplete()){
            fieldItem0.setVisibility(View.GONE);
        }

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_receive;
    }

}

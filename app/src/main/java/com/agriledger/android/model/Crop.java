package com.agriledger.android.model;

import com.agriledger.android.util.L;

/**
 * User: Pavel
 * Date: 29.08.2016
 * Time: 18:01
 */
public class Crop {

    public String cooperatorName;
    public String farmerName;
    public String productName;
    public String fieldName;
    public String harvestDate;
    public boolean cooperatorVerified;
    public boolean farmerVerified;
    public boolean isReady;

    public static Crop createRiverFieldCrop() {
        L.i("createRiverFieldCrop");
        Crop crop = new Crop();
        crop.fieldName = "By River Field";
        crop.productName = "Wheat 5 bags";
        crop.harvestDate = "2016/10/23";
        crop.isReady = true;
        return crop;
    }

    public static Crop createForestFieldCrop() {
        L.i("createRiverFieldCrop");
        Crop crop = new Crop();
        crop.fieldName = "Forest Field";
        crop.productName = "Wheat 4 bags";
        crop.harvestDate = "2017/06/19";
        crop.isReady = false;
        return crop;
    }

}

package com.agriledger.android.model;

import com.agriledger.android.util.L;

/**
 * User: Pavel
 * Date: 29.08.2016
 * Time: 14:02
 */
public class Cooperator {

    /************************************
     * PUBLIC FIELDS
     ************************************/
    public String name;
    public String id;
    public String description;

    /************************************
     * PUBLIC STATIC METHODS
     ************************************/
    public static Cooperator createCooperator() {
        L.i("createCooperator");
        Cooperator cooperator = new Cooperator();
        cooperator.name = "John";
        cooperator.id = "1ld83nd73b6d";
        cooperator.description = "Kenya farmers";
        return cooperator;
    }

}

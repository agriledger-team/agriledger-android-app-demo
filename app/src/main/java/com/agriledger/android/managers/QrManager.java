package com.agriledger.android.managers;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import com.agriledger.android.util.L;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.qrcode.QRCodeWriter;

/**
 * User: Pavel
 * Date: 29.08.2016
 * Time: 15:36
 */
public class QrManager {

    public static Bitmap generateQrCode(String s) {
        L.i("generateQrCode", s);
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(s, BarcodeFormat.QR_CODE, 512, 512);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            return bmp;
        } catch (WriterException e) {
            L.e("generateQrCode - WriterException - " + Log.getStackTraceString(e));
            return null;
        }
    }

    public static void scanQrCode(Activity a) {
        L.i("readQrCode");
        new IntentIntegrator(a).initiateScan();
    }

    /************************************
     * PUBLIC INTERFACES
     ************************************/
    public interface ScanQrCodeListener {

        void onScanSuccess(String result);

        void onScanError(String errorMessage);

    }

}
